FOR1  �BEAMExDc  �hd elixir_docs_v1l   hd docsl   	hhd asyncaa/d defl   hd 
supervisorjd nilhd funjd niljm   �Starts a task that can be awaited on.

The `supervisor` must be a reference as defined in `Task.Supervisor`.
The task will still be linked to the caller, see `Task.async/3` for
more information and `async_nolink/2` for a non-linked variant.
hhd asyncaa;d defl   hd 
supervisorjd nilhd modulejd nilhd funjd nilhd argsjd niljm   �Starts a task that can be awaited on.

The `supervisor` must be a reference as defined in `Task.Supervisor`.
The task will still be linked to the caller, see `Task.async/3` for
more information and `async_nolink/2` for a non-linked variant.
hhd async_nolinkaaMd defl   hd 
supervisorjd nilhd funjd niljm   �Starts a task that can be awaited on.

The `supervisor` must be a reference as defined in `Task.Supervisor`.
The task won't be linked to the caller, see `Task.async/3` for
more information.
hhd async_nolinkaaYd defl   hd 
supervisorjd nilhd modulejd nilhd funjd nilhd argsjd niljm   �Starts a task that can be awaited on.

The `supervisor` must be a reference as defined in `Task.Supervisor`.
The task won't be linked to the caller, see `Task.async/3` for
more information.
hhd childrenaard defl   hd 
supervisorjd niljm   Returns all children pids.
hhd start_childaazd defl   hd 
supervisorjd nilhd funjd niljm  Starts a task as child of the given `supervisor`.

Note that the spawned process is not linked to the caller, but
only to the supervisor. This command is useful in case the
task needs to perform side-effects (like I/O) and does not need
to report back to the caller.
hhd start_childaa�d defl   hd 
supervisorjd nilhd modulejd nilhd funjd nilhd argsjd niljm   �Starts a task as child of the given `supervisor`.

Similar to `start_child/2` except the task is specified
by the given `module`, `fun` and `args`.
hhd 
start_linkaad defl   hd \\jl   hd optsjd niljjjm  �Starts a new supervisor.

The supported options are:

* `:name` - used to register a supervisor name, the supported values are
  described under the `Name Registration` section in the `GenServer` module
  docs;

* `:restart` - the restart strategy, may be `:temporary` (the default),
  `:transient` or `:permanent`. Check `Supervisor.Spec` for more info.
  Defaults to `:temporary` as most tasks can't be effectively restarted after
  a crash;

* `:shutdown` - `:brutal_kill` if the tasks must be killed directly on shutdown
  or an integer indicating the timeout value, defaults to 5000 milliseconds;

* `:max_restarts` and `:max_seconds` - as specified in `Supervisor.Spec.supervise/2`;

hhd terminate_childaajd defl   hd 
supervisorjd nilhd pidjd niljm   +Terminates the child with the given `pid`.
jhd 	moduledocham  �A task supervisor.

This module defines a supervisor which can be used to dynamically
supervise tasks. Behind the scenes, this module is implemented as a
`:simple_one_for_one` supervisor where the workers are temporary
(i.e. they are not restarted after they die).

See the `Task` module for more information.

## Name Registration

A `Task.Supervisor` is bound to the same name registration rules as a
`GenServer`. Read more about them in the `GenServer` docs.
hd callback_docsjhd 	type_docsjjAtom  �   ,Elixir.Task.Supervisor__info__	functionsmacroserlangget_module_infoasyncselflinkElixir.Supervisorstart_childokprocessmonitorsendpidref
__struct__Elixir.Taskownerapplyget_infonoderegistered_nameElixir.Processinfochildrenwhich_childrenElixir.Enummap
start_link	temporaryrestartElixir.KeywordpopshutdownElixir.Task.SupervisedElixir.Supervisor.Specworkerasync_nolinkterminate_childmodule_info-children/1-fun-0-element   Code  �          �   )   � " 0U;U@25BE0@G @@P@@� N  `�r@pP@	C@4@C@3@#@$@D� ��F0#G$GGE##E##E�##ED#@4$4� 0�0  9�:� B B#+��@#@�@0@@��P @0@F G$G@�` P�p�G �




$0��0H���r �  EG 3@
#@R@u���
�	`@
�� p9�:� B B#+�
4�#	=��@#�0F GG@�J��
���@g @@�� ����@@@	C@4@C@3@#@$����F0#G$GGE##E#@4��  @��
 @��
@
 #@
!�0�9: B B@
$@�#�0�9: �B B#F G
$GEF 3G
!G@#E3#@@
%�0�@EEG0� ��H�H��   EG 3@
#@R@�
(   EG 3@
#@R@!�
) 1�N � �
(@!P@	C@4@C@3@#@$@D���F0#G$GGE##E##E�##ED#@4$4� 0�  9":" B B#+"�@#@�@� @0@F G$G@� P��G �




$0"�H#� 
* $@� N�%� 
*&@@� N  '��
+(���!StrT    ImpT   �                         
            	                                           
                  "   #      &   '      
         
   )                  ,   ExpT   �      *      &   *       $   (      !   )         (                                                             
                  FunT         +      (        T͂LitT   �  x�]OQ
�0�Ƅ1�������Z]���6��ޅ<�i���I^^�{$ P�{q\��԰Qq��*p��N0��Y?���U�v��`�*Ҹ����ٳb��dF2����=�0Z��|�z���ׂ���³%Dο8���|Uq�u-�}2�����t�5����ަ���kP� LocT         +      (         Attr   (�l   hd vsnl   n �/���*��R���jjCInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0.3hd timehb  �aaaa8a8hd sourcek Y/private/tmp/elixir-20160502-35177-1k3k3wg/elixir-1.2.5/lib/elixir/lib/task/supervisor.exj Abst  <�P  *�x��ZKS�H��@H%���a �"��Z��"�G��U�ڱ4�e�pI2���a/��{wF�F�3#�f}J2��������ӫ��ې�LҎ�X�y��5�^�6�H�N{Sb�Ɗxǥ���L2���AK[+�Q�}����u��c]����8Q�ѯ���iz�=��ߵ\��]�c{_�1��?LK4�V��c^�h�[���M�A�����Zg���*Z]�L��/�^�6�C�15��r�/�Ix���g����W�~i��Ë�9��$�]'�U��1�>�9���?����S� ����F�#�J�k	��V���E�L��F�b���(&alZ=ǵ�0zв�PF�b�AFs��#��.VT�k�}��7���&iX������(�m,��J0����t	�����7T�G����H���,n��7G��9�;n�
ʏ����o�\M/��;C-�ò��?�����E�%��-����z���ȭ��9�C4��������X�	O˄WƄ�,&�ԟr)T���_2�[-��2P��u+ܻpݿ͍o޲ѯ���"֓�5M�����;�L�Ҩ�M�&��`���/C����R�H�iE�ӊ\�����g�__�g믄���7^�r�w�1K���C�M�Xa��j����)���O�!熚�f�"��AH0v�F��f��%�k��v�ӵR����Ǐi���{��?%6ip0�����a��I�?"�2����7�Zj��$��i���$b�"�����G��r3��,`�(����"`�x9�)�5Ib,C����Pe�U���ܲ�X�U�4����nH�tT<�Pv���mNo�)�L]�w1�.��iK�S�7QP-������4�Dמ�`[_g�������:��`Z�J�����)+���Gb�G�Ůc����+|C����:��u;} �%ķR#~��(�����B_=�H�Q���19��g��*:9a�=Qz@%�������b0pR�(�#JE����!rl'�'s7l'����}<�N���!p:s����r&>����v`؝A�g�:��/�8Uzy�d�6)�9>��΋��'A�K���R��N������7A�#�kp�޲����I�(��g\�M3�C��f��\o��A��92��A�Ò���x��P+��@�>�Z{��t��i�+w�af�a��C���{�Aw�+��� ��Y��nG��o�2�/@�7��lw������w�>ߋ�}g�����4?e����'$��̿g�<��4�`�����>i��hHm�'�OO�s�IA�K��!=��^������R~09|����]�>��r���n��)�F���6q[���>���P��1E�!�f�s���B��d�/������	�Haa�4�5s1�J��+ܥ����Ue����5 �qyz̫y���Qi���`�@쓹=6)j_&����p��I=�<�)��d٩m'�}�踛(�Mt����Mi �xtSa~3����Q%�����a��7�A,���]S����i�6�B�h�l��aK�ݶ��'���Q�ٷ-���$!_+3;��ZL�O�$�6v;�6'�7�m�� D۩�? �m�h{>D�:DB��H�jj.(m���_gG�a֧�2'�v��;�gq��{_��S�a��5�I��MMm���k��ʂl�9�OГ9�3$39S���ݹ�ஒ�Yt�b�4�Sr�W�;;��C�ba�V��0��� �BT��4��r����;��.�
��m��3����"����|߿������r�	��#�2���ŕ�3r�<F�^ʈ��� �Ǥ�y���U�����a���h����L��2
�̝܁� F��-�U��ߕ ���SBt�Q�"�~Mp��Ů�n�؆�������-!�*�-Z��t������e����/jKǯ��k��C@�]\��x�֌�l�{�ص��s;/�l<^�!��TRh-��� �j��@m�bm� �S>�h�� ��?���Ń@,���@� v�ҵ��ݒA�����?<T�Line   g           -      	C	E	F	G	H	I	J	7	�	�	v	w	�	�	'	)	*	+	,	�	U	n	o	a	c	d	e	f	g lib/task/supervisor.ex 