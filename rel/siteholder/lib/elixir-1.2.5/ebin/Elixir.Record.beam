FOR1  E�BEAMExDc  ��hd elixir_docs_v1l   hd docsl   
hhd 
__access__aa�d defl   hd atomjd nilhd fieldsjd nilhd argsjd nilhd callerjd niljd falsehhd 
__access__ab  d defl   hd atomjd nilhd fieldsjd nilhd recordjd nilhd argsjd nilhd callerjd niljd falsehhd 
__fields__aa�d defl   hd typejd nilhd fieldsjd niljd falsehhd __keyword__ab  hd defl   hd atomjd nilhd fieldsjd nilhd recordjd niljd falsehhd 	defrecordaa�d defmacrol   hd namejd nilhd \\jl   hd tagjd nild niljhd kvjd niljm  Defines a set of macros to create and access a record.

The macros are going to have `name`, a tag (which defaults)
to the name if none is given, and a set of fields given by
`kv`.

## Examples

    defmodule User do
      require Record
      Record.defrecord :user, [name: "meg", age: "25"]
    end

In the example above, a set of macros named `user` but with different
arities will be defined to manipulate the underlying record:

    # To create records
    record = user()        #=> {:user, "meg", 25}
    record = user(age: 26) #=> {:user, "meg", 26}

    # To get a field from the record
    user(record, :name) #=> "meg"

    # To update the record
    user(record, age: 26) #=> {:user, "meg", 26}

    # Convert a record to a keyword list
    user(record) #=> [name: "meg", age: 26]

The generated macros can also be used in order to pattern match on records and
to bind variables during the match:

    record = user() #=> {:user, "meg", 25}

    user(name: name) = record
    name #=> "meg"

By default, Elixir uses the record name as the first element of
the tuple (the tag). But it can be changed to something else:

    defmodule User do
      require Record
      Record.defrecord :user, User, name: nil
    end

    require User
    User.user() #=> {User, nil}

## Defining extracted records with anonymous functions

If a record defines an anonymous function, an ArgumentError
will occur if you attempt to create a record with it.
This can occur unintentionally when defining a record after extracting
it from an Erlang library that uses anonymous functions for defaults.

    Record.defrecord :my_rec, Record.extract(...)
    #=> ** (ArgumentError) invalid value for record field fun_field,
    cannot escape #Function<12.90072148/2 in :erl_eval.expr/5>.

To work around this error, redefine the field with your own &M.f/a function,
like so:

    defmodule MyRec do
      require Record
      Record.defrecord :my_rec, Record.extract(...) |> Keyword.merge(fun_field: &__MODULE__.foo/2)
      def foo(bar, baz), do: IO.inspect({bar, baz})
    end
hhd 
defrecordpaa�d defmacrol   hd namejd nilhd \\jl   hd tagjd nild niljhd kvjd niljm   4Same as `defrecord/3` but generates private macros.
hhd extractaa'd defl   hd namejd nilhd optsjd niljm  DExtracts record information from an Erlang file.

Returns a quoted expression containing the fields as a list
of tuples. It expects the record name to be an atom and the
library path to be a string at expansion time.

## Examples

    iex> Record.extract(:file_info, from_lib: "kernel/include/file.hrl")
    [size: :undefined, type: :undefined, access: :undefined, atime: :undefined,
     mtime: :undefined, ctime: :undefined, mode: :undefined, links: :undefined,
     major_device: :undefined, minor_device: :undefined, inode: :undefined,
     uid: :undefined, gid: :undefined]

hhd extract_allaa;d defl   hd optsjd niljm  ;Extracts all records information from an Erlang file.

Returns a keyword list containing extracted record names as keys, and
lists of tuples describing the fields as values. It expects a named
argument :from or :from_lib, which correspond to *include* or
*include_lib* attribute from Erlang modules, respectively.

hhd 	is_recordaadd defmacrol   hd datajd niljm  
Checks if the given `data` is a record.

This is implemented as a macro so it can be used in guard clauses.

## Examples

    iex> record = {User, "john", 27}
    iex> Record.is_record(record)
    true
    iex> tuple = {}
    iex> Record.is_record(tuple)
    false

hhd 	is_recordaaHd defmacrol   hd datajd nilhd kindjd niljm   �Checks if the given `data` is a record of `kind`.

This is implemented as a macro so it can be used in guard clauses.

## Examples

    iex> record = {User, "john", 27}
    iex> Record.is_record(record, User)
    true

jhd 	moduledocham  !Module to work with, define and import records.

Records are simply tuples where the first element is an atom:

    iex> Record.is_record {User, "john", 27}
    true

This module provides conveniences for working with records at
compilation time, where compile-time field names are used to
manipulate the tuples, providing fast operations on top of
the tuples' compact structure.

In Elixir, records are used mostly in two situations:

  1. to work with short, internal data
  2. to interface with Erlang records

The macros `defrecord/3` and `defrecordp/3` can be used to create
records while `extract/2` can be used to extract records from Erlang
files.

## Types

Types can be defined for tuples with the `record/2` macro (only available
in typespecs). Like with the generated record macros it will expand to
a tuple.

    defmodule MyModule do
      require Record
      Record.defrecord :user, name: "john", age: 25

      @type user :: record(:user, name: String.t, age: integer)
      # expands to: "@type user :: {:user, String.t, integer}"
    end
hd callback_docsjhd 	type_docsjjAtom  (   PElixir.Record__info__macros	functionserlangget_module_infoMACRO-defrecordnilMACRO-defrecordp
find_index+updateElixir.Macro.Env	in_match?falseElixir.ArgumentError	exceptionerrorElixir.Enumreduce=	__block__MACRO-is_record
elixir_envlinify	in_guard?true
tuple_size>is_tupleis_atomandextract_allElixir.Record.ExtractorindexElixir.KernelinspectElixir.String.Chars	to_string	byte_sizeall-
__access__Elixir.Keywordkeyword?Elixir.Macrojoin_keywordlistsreverse
__fields__mapextractgetexpand{}lengthlist_to_tuple__keyword__escapecreate
map_reducehd==elementtuple_to_listmodule_info-create/4-fun-1--create/4-fun-0-has_key?deletefunction_clause-__fields__/2-fun-0-badargElixir.Exception	normalize__exception__
__struct__messagebit_size-update/5-fun-0-Code  ^          �   �   � " 0U;U@25BE0@G @@GP@@� N  `�r0p@#3@�#@9�� �0�@#3@�#@��0�0�8�A3C9�3:�3 B3 S+�S�0}0#!��@}P##@C0��4�@���P�P�@P@@C@@3$@#4�` ;@��@G �p0�p@@@g @4@#@$��0P@� �@"@EEG0E#EG@E3#EGP##F03G
GG#E3G`#F03G
GGE3#F0#G
GGE#F0G
GG��
  @��`��p0;@
�0 EEF0#G
GGpGE#G�F0#G
GGpGE#F0#G
GGpGE#EEF0#GG�GGE#F0#G
GGpGE#F0#G
 GGpGE#F0G
 GGpG� EEG�F0G
GGEG�F0G
GG��J��
!7��N���
#0 0@@@#@#@��0�;"@��@���5=���@@���5 =! ���!��@| �#o#| �#o#o	m  \p Z
)� \pZ
)� @��0��@"�}� #�
+P$0%3@1%PP@D@3@C@@#$@4��,&�,&�@$#@3@4@C@DP�P&@$4D��5'=('��(�@| �#o#o	?m  \?Z
)� @�0�@)�
/0*8+A3C9)3:)3 B3 S8)P`AcF 3GSGcE3##@C0*+4)4)@#�N�,�
2 - @g@� .�
4 /0.7.�N 0�
5@10@@$@#@#@@3@�0�;7@�2�22@$$��53=43��4@$@��55=65��6�@| �#o#| �$#o#o	m  \p Z
)� $\pZ
)� $@�0�@7�EEF0GG�GG08�r@9"@EEG0E#EG@E3#EGP##F03G
GG#E3G�#F03G
GGE3#F0#G
GGE#F0G
GG:�
+@;0<#0<@@@4@#@3@@$��,=�,=�@#@$@3@4@B@=@@�  9@<@@0> ?>B B #+@
78@#A#3C+@34|@PCS|@`$c+@Sc PE4C�  �!@@#@�  �"0�" ?B B#+@4|@0$3+@300F 3G4G#@$@3#@4�@ �#0�# @� EE$E4F0GG�GG@A�$
<@B0@@$@3@#@�% g @@#@�&09H:H B B#4C#` E$F0G
7GG0C@#g0@@#@�'0P�'�@@$@$�(�5D=ED�(�E�(
$@$@�(�5F=GF�(�G�(@| �#o#| �$#o#o	m  \p Z
)� $\pZ
)� $@�(0�(@H�)HI�*
0J 0@#@�*`�+p0M;M@
K�LK2 EEF0#G
GGpGE#G�F0#G
GGpGE#F0#G
GGpGE#EEE#F03GG�GGE3#F0#G
?GGpGE#F0#G
 GGpGE#F0G
 GGpG L  EEG�EEG�F0#G
?GGpGE#EG�F0#G
 GGpGE#F0#G
GGE#F0G
GG M�+JN�,
:0O00@#@@$9P#�-
#3'P3�-*U$P@$$�.�5Q=RQ�.�R@$@�.�5S=TS�.�T�.@| �#o#| �$#o#o	Sm  \=^Z
)� $\�Z
)� $@�/0�/@U@$� �08VA#@#@#@0* V�0HW� 
B X@� NY� 
BZ@@� N  [�'
C \9]:]   B #E#]@^�&
D0_9g:g @0B 3B@@3@#@$@4�1 0f;f@�`
c`0e;e@�a
ba@�2=db@G=dc@4@$�3 d@#@4@$@#4�0�4 0F GG@e�5Jf�1Jg@ E#E#@
G�&  h�
H i9q:q B #B30q# @hj@3@#�6i0F GG@ jj+p
,k
I
m3+m3!m3+m3
Ik@
�7 !@5l@=ol@�8�=om�p�p@
LC
M3+pC
+p3
@
�7 !@5n@=on@�8�o@@@�8"�8@| �#o#| �#o#o	!m  \�Z
)� \ WZ
)� @�80�8@p� l# q0r0F GG�@r @5s@=ts@�9�t@@@�9�5u=vu�9�v�9@	�| ##o#� � \�Z
)� @�90�9@w��
P@x9: @@B CB$@@C@#@#@3@4�:0�;~@�y�yy@$�;�5z={z�;�{@@4@4�;�5|=}|�;�}�;@| �#o#| �4#o#o	m  \p Z
)� 4\pZ
)� 4@�;0�;@~�E$EEF0GGGG@@ E#E#@
G��    StrT   �record  does not have the key: expected arguments to be a compile time atom or keywords, got: expected argument to be a literal atom, literal keyword or a  record, got runtime: invalid value for record field  fields must be atoms, got: ImpT  �   $                                                                           "   !      $   %      &   '         (         *      ,   -      .   '      0   1      0   3      "   4      .   6         8         9         :      .   ;         =         >                  @         A               ,   E      ,   5      ,   F               J   K      J   N         O   ExpT   �      B      Z   B       X   :      O         J   +      ;         9   4      /   2      -   +      $   !                  	         	      	                  FunT   d      P      x       ��5   H      i      ��5   D      _      ��5   C      \       ��5LitT  r  
x��W�o�0w��]���R����؅3����N��a�e���8����g;]�6)EM�!���=���{�{E=G�R��?�c4��D�(S1�W��ُ���X&X�!+pu�[��̈��cE�(`��0aTĆ`�=�8�7�@P���֊D��W;L� ����L��!"Rfz��U���<����B�:NI�2�,6����rĨ'IJy���
v����/-;�d��}a�~r���[ <#1���Ȣcnh=C�>��~�~����	 �Lj{Mx}��Zb}�JR�����9_u���Ӡ�Jג�:")($�! ��너�Za�V�� w��\��OS�6�y`�����g�rtNw��5�5H=���`r^+��;l�����bc��fٶr����������9����Lw�݆+՜ۇ�l����{�[d������)�&�2A��M�.��J95-DДJm�_�U<�\����Q?��qG�pg�����y.���M@�>pҍ�#�0.��}{M�{L��Yڅ����͎�b�F��F�z\�<-��#�%��`�����0�c.�^v����I��k�уv.�Ck��B�\]��	$3�޶�}���EN� ���  LocT   |   
   P      x   H      i   D      _   C      \   <      B   5      1   /      *   #                  
      Attr   (�l   hd vsnl   n $��ɹ��X�屆:�jjCInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0.3hd timehb  �aaaa8a7hd sourcek P/private/tmp/elixir-20160502-35177-1k3k3wg/elixir-1.2.5/lib/elixir/lib/record.exj  Abst  ҃P  �Tx��=�o�F��$�_M[ಉspj��i�8��t�im4�,�d�C�끠Eڦ,Q>�r�������?��	���қ��yo>HJrl��/���7����y3ܫ:��;����nE��Z�R�,�՝ʂE��ݏ\��T�Z���q���~ϲ�Q��� B]g�J}��۫�W�u��O���U�Ȟ��6�=�O����xG^��8�����(r��(֏6�~��뎻���5ek��~�ZXg��.��B+�QU�� -�n��0$��q�lҰ��6Ā�-��wz����dY���D�Üs�bQ��K�/��t왶�����à)�bvףq��Nߧ�U�:w�"��l��z��{5�nG�.m_䣇�=��Ȥ{.LZ�]��`[�묣N����};�]�v��p��m%�� �Z�x���l�?踅 �l�Ƌ�]��k�E�¦9Ϗ�]7h�{�D�)c��"e���V��Ĝ���Q(�vƀ�mWhG�,{�
�T���x�����R@R[C@�Ł���ԊCI�x<�GP� �^�]i��5���B]�^qp
�0J�q1�΃#�R���ei��/�ڡPuj�@�42T蟍���h�����m캑�\i�%dTQ<�@�*$3$r^2W$@��6� �����ƚD��Y�ȴs��(�^�{Ű��W�dB"�ը�|������&H����x�C��!�[��O��O�J ��3����-��U�2n������k����A:�Rt8EG����"��#��@:R�Ǐ�В��,�r化����q���ic�$
�O"���-DU�&���O�k"����YlL�s�8+Bߠ@���p�@j�%J�I���P9�AJD5���>��}���"�&n��«[�!��}|�kG�����O�fx04 [�^�YI�l��_l����] �e�~kv�V	��B�JaF��1��=6n���'��v�]׏�(z�=j�Q2A`4l{~�	�K��v\
��O�̶=׮�W��l��EM�?���Mf�0z�q�q�F���C-!�Jܿ��D�_,��z��z 3˄�-���<�_"�AWr1t�� �1j6��2�*�~�����R�dDi��C��w����d� �u6f}�P��0vwč�"�V<4{܇G�PDZ�6� ��we����d�bk��6�G���� 0y������|�T�q��>�[�X��>�Zi���Q�P�8��α����B���ԙd�M�ײ���\�u}�!��Щ}�w�%Os�n�L1Q*�4I���N)�GS���g�V���#>!��K&�*�MϨ�^h�k�	%o�7�J�ᯱ��~�g�dQ�bڬ���t܉�x�.b!�M��M���C���M��0�&����-f}�-f]o1�#���O�b>7YL�0�γDeD5e�`fU���٫$^���Ѝ���t`S����(U�� $��0�r5[��RW��d�!����כn=��u�e�W���.l����^��Mtv߲�L3�� /�)p>�Y��+�1u���@��b\#�
�D@�
����P���3��^���{�_|��t�ܣh��!�F�!��z��N9TƂUN��տ����!z��iɲ�G�C��WT1f�WT3��3��2_��X �*��F����B�P�֓9����O���ƅ0 7�p�~;�v�;�[�^�UƢl�:�����͉�~��8���rl ��#L"Q6[�_1��j#���߶�oȝ�&}-��S7?u����O�~N1x(��ks��`ZZ���.}I^�#�ǎl�&.3���Hj��KzdK��U��nL��
��h��v��5@-۴��l�H	:l}���R�ݾ8�ԃ>H�v�>�iP�^dy�f����g��M�p�����;n�-O}9~d|p��
��M�4Ų�!W���ӿǏ[�v�n���X˩�R��X/F���QH*�;K$B���`�}���l��&��1�����i��x}�Z0����O@5����=|vYZ����F� 
��>��'N��)����1���*Ɛ�Zm�l�.Ƭn1��"c{RG�G��|D&��¥�q)��S)Ͼ|�)I'�sq;>h��_�VY�O��+�R�+��T���+^��(�\<M ��\�u�7�hO �<��|K_U�*�ۤ�q}�X~�Uc�Uڢ������*W9&�j٢^3ᆢޫ��ޫ'RՑNg���#�LV���)�z�G�d�g�M*��������[�[��u�g�dx0��ܚh�����[#���S������ʀ�U�g1��������B^�2a)��ϣ{yTi�y���W�Y�5�mgQQjo��>�7<b����]@»��C\��\�/p��y��n:Y�;���$�)��� ͋�Ǯ��C@��5���a�ŌP�{�y���0��H� ��ܯ�ge�����|�{�,�e�B�Ze��"m�!���w񻺬�Hw�WH#��j�%��K�& �/)��F).i���/{D��g�����A�WD�+"�i���e���
J �r�hv�����a3�5�ݦ�Ln�jF^��$,i��f�Y�'��^T<�X6�XA������#$�b)+zs"@�Ltn�R��3�G�d�277kCn���y�y��7B-"��sc�e�|�,�~Y=_O�q9%@yj����{����V�pl	�++r�M�m��E+�	��!]��іM�7��Pj�{�L� #�i}*��H� o"5o"ͭQFI���(����p�t���,]��w��u�C7`���a6Z��]q)����D�7���!�(H@Z��I\���D���׵����*��\��3�T)S�g]B�YrL�?���l��2I�F宼A�D���p����R �ڿPD} �L�]"����=�zS��_=!��w�B���k���:u�m!٩0�ka	2��t>pI�*�T��U��Ķm;$���u��(��fxv!!�9>m4˭�h�t���)���|�k5_�o�\!���KK'��p0]�{A��$,Bu����T��8�3_ިù	�9�S��;��(��xa2�/�O��s�qi�t?Y���f�d�P6�O��&��e���^�IkkH��[bLJj̽cU'��X�W�%�~RNGͤ|Av�놡���T����
e�`�}IBX��-~@~��r�������ߊ,����u�β�sa к݀�R��7QdT��i<�ƃi<�ƃ��($�� �:�`x&�V����Ǽ���J3A�\�ŹXn�D���2t/ړ7�	�"	�f�5n� #�M���1��m9ƣ�Q��cy�fR\ZJvii�ȸw��R�]ڱޥ�J��c��.��6��0��iȠevcS�"|�{P�����)��&2�k�����&���5<���p��m�F���m�Hg|=�mGU��Sn�q�'�E��6��ۺța�
�\�,$��8�0@~�Ӑ�wd��qL%�Ϝ�.C�c1~��(�lw��rg��fc噣���Y���#}�\��\S���s"�e�J~�A�S��o�w�1_���p�\�L{�����K_�l��#˨�q�F�X�F�e,\���f>����0!9�pC�e�´��(\��I.�H�SG��F�Feq�Yr�#,cq�\�2����ϬɌ�o˙��:�Ō�l�AS�-��)[���x�w�3�)������-O�Lg�T}���~	F��|��K9E}��{�I�����3f�r������^����y�l�W9��O�C�{�s�Ҹ^�8н���kGS�����	0��u���Lo�_3��uz+��bթ��M{�ӿTx��^~|�����)��x]�Wxၼ$�+\��U���M�)�O��L7T�z�ZυG�U��3��f���B&d�M�:Q��;�"5U�)Ǭz�����j�����ڂ�L��ڂ���8�tmA�tmA�,�x!f�ټ��6���M����Mn. �u�����%#K{ko(��؜mH�*� �7�J4 ����j�GD\<��X6��7Dܙu@���z>oC����/��w���9%�i+-�A�7���9x�.nB\��uY�(���I�" p����X��@�����H�#��s�"��Q��.�TPѼRD�C���ʍ"����Z������a����6�%E�%O4�bA�Yި\Tn�s]T�H��`Wk$���,"�,�ʢ^E���O
����^B(/ԋ%�^�a�b)G/���ZBz�����TΒ�'I�,������Q�o���s����8��e����5�c�ɨ@?��������m�����p.���5��52��|+~�|�5d�׀$�_�I�.�`�|���"D���ӃH	��y��i{����`��
*=������U�zf4p�~˕|ɪF,��X�R�a��
���Z-���QV�.�����}�n�Y%���T|�S�}�;ZC��� ���X{��t�aߞR���ȼ!a��joϤ�׌Vj�k�	Xs��{ �5#�k�5��e����P?6��ɭ��9�-Eѭ�H'�(��b�0�#�� ��,�#��CZ��B���/z_���7rU#��QI�mX3�G����SI�.���ρ��� 3�ʗ�e^�R��ܺ�Lr��*ˡꐎ��FF�(�$� 4��R�|�S)�����/P�)_3"�����
��F~T�p��BI#a2ja��tT�w%8�F����U�X�TO�vؐH��@��H�����M��3����ƣ��u��8C���·��\��pC���}�D�nl:�z�R�Na#�Q�Q:d������Bt>n���B�>�q�LfhwV�s����|���j�>�ַ�����Z��d}��)�vj��@-�x�����% �	H��K�!�'�b�ش�����D������;c��G��8�*��
U~���T�i�������Os>���E畿��!N=c�q�Xy�9ӏ*��q�Hcai��c�|on�uv�L�}O�y��,k�����oy�F��ז�}(�7il���|��ٝ�&��}my� ���ĵ"�VN�j:�z��?3�I9�r���Tx�M��e�qV�T��=����w����(+GkK�øU\J�����Ķ��ţ��;���:�a��}�����F�oD`��6�nWp$	܌F{�;� 0����h��mT������;o�v[+~WX��+}���E�͝�"�O��Ď�'���+���+�>�_��+��a3�?��0�7�R����Q�Q�fV���x߸��-����Br#~|o3���c�ŷ�ά�dAv�&3�;X)uZ������z&c��R�ZYITn.U�݋C��ņ_ �c��m����3jK�C_�b�_��3~���d�b,��h� ��)>��F���	�����\�P�X�I�L��8�����&%8��qh"g����}����tD$PI��eۉb  Line   �           �   ;   	�	�)c)d)F)G)H)K	s	t	D	E)!)")%)#)))))s)v	�	�	7	8)X)Y)_	�))))	))*)+).)@)A)-	T	U)i)j)n)o)k)0)5)1)9)3	�	�	�	�)L)R lib/record.ex  