FOR1  ?�BEAMExDc  +�hd elixir_docs_v1l   hd docsl   hhd __before_compile__aa�d defmacrol   hd envjd Elixirjd falsehhd 	__route__ab  Fd defl   hd methodjd nilhd pathjd nilhd guardsjd nilhd optionsjd niljd falsehhd 	__using__aa�d defmacrol   hd optsjd niljd falsehhd deleteaa�d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm   aDispatches to the path only if the request is a DELETE request.
See `match/3` for more examples.
hhd forwardab  d defmacrol   hd pathjd nilhd optionsjd niljm  �Forwards requests to another Plug. The `path_info` of the forwarded
connection will exclude the portion of the path specified in the
call to `forward`.

## Options

`forward` accepts the following options:

  * `:to` - a Plug the requests will be forwarded to.
  * `:host` - a string representing the host or subdomain, exactly like in
    `match/3`.

All remaining options are passed to the target plug.

## Examples

    forward "/users", to: UserRouter

Assuming the above code, a request to `/users/sign_in` will be forwarded to
the `UserRouter` plug, which will receive what it will see as a request to
`/sign_in`.

Some other examples:

    forward "/foo/bar", to: :foo_bar_plug, host: "foobar."
    forward "/api", to: ApiRouter, plug_specific_option: true
hhd getaa�d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm   ^Dispatches to the path only if the request is a GET request.
See `match/3` for more examples.
hhd matchaa�d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm  �Main API to define routes.

It accepts an expression representing the path and many options
allowing the match to be configured.

## Examples

    match "/foo/bar", via: :get do
      send_resp(conn, 200, "hello world")
    end

## Options

`match/3` and the other route macros accept the following options:

  * `:host` - the host which the route should match. Defaults to `nil`,
    meaning no host match, but can be a string like "example.com" or a
    string ending with ".", like "subdomain." for a subdomain match.

  * `:via` - matches the route against some specific HTTP method (specified as
    an atom, like `:get` or `:put`.

  * `:do` - contains the implementation to be invoked in case
    the route matches.

hhd optionsab  d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm   cDispatches to the path only if the request is an OPTIONS request.
See `match/3` for more examples.
hhd patchaa�d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm   `Dispatches to the path only if the request is a PATCH request.
See `match/3` for more examples.
hhd postaa�d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm   _Dispatches to the path only if the request is a POST request.
See `match/3` for more examples.
hhd putaa�d defmacrol   hd pathjd nilhd optionsjd nilhd \\jl   hd contentsjd niljjjm   ^Dispatches to the path only if the request is a PUT request.
See `match/3` for more examples.
jhd 	moduledocham  �A DSL to define a routing algorithm that works with Plug.

It provides a set of macros to generate routes. For example:

    defmodule AppRouter do
      use Plug.Router

      plug :match
      plug :dispatch

      get "/hello" do
        send_resp(conn, 200, "world")
      end

      match _ do
        send_resp(conn, 404, "oops")
      end
    end

Each route needs to return a connection, as per the Plug spec.
A catch-all `match` is recommended to be defined as in the example
above, otherwise routing fails with a function clause error.

The router is itself a plug, which means it can be invoked as:

    AppRouter.call(conn, AppRouter.init([]))

Notice the router contains a plug pipeline and by default it requires
two plugs: `match` and `dispatch`. `match` is responsible for
finding a matching route which is then forwarded to `dispatch`.
This means users can easily hook into the router mechanism and add
behaviour before match, before dispatch or after both.

To specify private options on `match` that can be used by plugs 
before `dispatch` pass an option with key `:private` containing a map.
Example:

    get "/hello", private: %{an_option: :a_value} do
      send_resp(conn, 200, "world")
    end

These options are assigned to `:private` in the call's `Plug.Conn`.

## Routes

    get "/hello" do
      send_resp(conn, 200, "world")
    end

In the example above, a request will only match if it is a `GET` request and
the route is "/hello". The supported HTTP methods are `get`, `post`, `put`,
`patch`, `delete` and `options`.

A route can also specify parameters which will then be
available in the function body:

    get "/hello/:name" do
      send_resp(conn, 200, "hello #{name}")
    end

Routes allow for globbing which will match the remaining parts
of a route and can be available as a parameter in the function
body. Also note that a glob can't be followed by other segments:

    get "/hello/*_rest" do
      send_resp(conn, 200, "matches all routes starting with /hello")
    end

    get "/hello/*glob" do
      send_resp(conn, 200, "route after /hello: #{inspect glob}")
    end

Finally, a general `match` function is also supported:

    match "/hello" do
      send_resp(conn, 200, "world")
    end

A `match` will match any route regardless of the HTTP method.
Check `match/3` for more information on how route compilation
works and a list of supported options.

## Error handling

In case something goes wrong in a request, the router by default
will crash, without returning any response to the client. This
behaviour can be configured in two ways, by using two different
modules:

* `Plug.ErrorHandler` - allows the developer to customize exactly
  which page is sent to the client via the `handle_errors/2` function;

* `Plug.Debugger` - automatically shows debugging and request information
  about the failure. This module is recommended to be used only in a
  development environment.

Here is an example of how both modules could be used in an application:

    defmodule AppRouter do
      use Plug.Router

      if Mix.env == :dev do
        use Plug.Debugger
      end

      use Plug.ErrorHandler

      plug :match
      plug :dispatch

      get "/hello" do
        send_resp(conn, 200, "world")
      end

      defp handle_errors(conn, %{kind: _kind, reason: _reason, stack: _stack}) do
        send_resp(conn, conn.status, "Something went wrong")
      end
    end

## Routes compilation

All routes are compiled to a match function that receives
three arguments: the method, the request path split on `/`
and the connection. Consider this example:

    match "/foo/bar", via: :get do
      send_resp(conn, 200, "hello world")
    end

It is compiled to:

    defp match("GET", ["foo", "bar"], conn) do
      send_resp(conn, 200, "hello world")
    end

This opens up a few possibilities. First, guards can be given
to `match`:

    match "/foo/:bar" when size(bar) <= 3, via: :get do
      send_resp(conn, 200, "hello world")
    end

Second, a list of split paths (which is the compiled result) is
also allowed:

    match ["foo", bar], via: :get do
      send_resp(conn, 200, "hello world")
    end

After a match is found, the block given as `do/end` is stored
as a function in the connection. This function is then retrieved
and invoked in the `dispatch` plug.

## Options

When used, the following options are accepted by `Plug.Router`:

  * `:log_on_halt` - accepts the level to log whenever the request is halted
hd callback_docsjhd 	type_docsjj Atom  �   ?Elixir.Plug.Router__info__	functionsmacroserlangget_module_info	MACRO-putputMACRO-deletebuild_methodsElixir.Plug.Router.Utilsnormalize_methodmake_funElixir.Enummapin
MACRO-postpostMACRO-matchdeleteMACRO-__before_compile__MACRO-optionsoptionsMACRO-patchpatchextract_path_and_guardswhentrue	__route__nilfalseviaElixir.AccessgetElixir.Listwrapbuild_path_matchhostbuild_host_matchMACRO-forward=	__block__extract_path_join_guardsandMACRO-__using__usecompiledoElixir.KeywordpopElixir.ArgumentError	exceptionerrorElixir.Macroescape	MACRO-getextract_private_mergerprivate&	update_inmodule_infoCode  a          �   J   � " 0U;U@25BE0@G @@GP@@� N  `�r@p@�@8�� �0�@3@��0� �8�A#34�30@#@�@0F GG@�4�0 F GG G�  @@@�@#@��P0 @@��P 0�EEG0#F0G
GG@G#@� �` /0F GG0G@ ��p
@�@
@8��
0@3@*�r0@3@u� �@@
@8��
 @GP��
@@
@8��
@@
@8��
9:0B B #+
8#A#3C8CACSc4c`@3@S��,0F GG@ ��,0F GG
@ ��
@ @@@#@3$@4;"@
!
!!@
 @3�� @"��P@�� �9$:$ B B@4@4�`9#:# B@$�C@#@
&@$@#$� @�ppF`GG`G4GGGG$@@#�H$��H%��
0&@3@'�
(0(5'0EEGpE#EG�F0#G
)GGE#G�F0#G
)GGE#F0G
*GG)��
@*@
@8+�
+,9-:-0B B #+-
,0-#@G�-.�
- /+0
0� EEF0G
.GG@G1�
/ 2 EEG�F0G
0GG�GEG�EG�EG�F0G
*GGG3��
04@3@5�p
06@3@�7�
1@8@@@4@$@
2@3@#� @,9
,9
0F GG@=;9@
2@� @,:
,:
@
2@� �=;:@G����;9=:= B B#@$@#$�9<:< B B@G� �@#@G@@#� �6E4EG0E#EGp##E$3EG�33ECEGCCESEGSSF0cG
)GGSEcGSF0cG
)GGCEcSCF0SG
)GG3ESC3F0CG
)GG#EC3#F03G
)GGE3#F0G
*GG@<�H=�H>� 
:@?@
"@8@� 
:0A@3@?B�!
;C7B @
<�" �;E@
D
DD@
 E�#�EEGF0GGGGEF0G
=GGEEGF0G
>GG@GEEG`F0G
)GG F� 
? G@� N�H� 
?I@@� N     StrT    ImpT   �                                          !   "      #   $         %         '      3   4      5   6         7      8   9      3   "      8   9            ExpT         ?      I   ?       G   :      A   :      ?         6         4   /      2         *   (      (         &                                        	                                    	      	                  LitT  I  "kx���j�FT��8mH)-�)�C ^�cڄ��m(�@���jV;cI��$_~�C�Ĕ����K?�_ҹ��iu��1��9�s�̙s4M�\Ӵ���՗��ݱ,J�Z`��?�t�/=��\
�9��r�'��d�#�0��(���e��O������$ ���K�5}��$��C���I���8���)�(��A�q�O&���diښna�呋�=�Ս��7��< #FKb+�^	cI	�ď�Y�T�y�l}?]�].��=SIK��S�������'��J`fN�1*����sQ&B�c�+�Na��J��L�6�޿i �"�f.�[�l$�"�Sh:� eq��X=��)H67��H��秄�P0��H5S[3"��2u���݀�[���0\��TNK��l@�	� �jh�}S���Hm�B�Gn?�ژ�n��Gvc� ��wk�"K6��C
P{tJ�����:���RRJ`c�!p��>�� �#hO�Gd�~gp���7!T<3�4\��{��C�e�3�J���{����4lG���ͬQ)���ϲ��cfJ�+Ot�)�߽�1r*� 	��S�%3�[B+b�mN5o��l�m�\�4+��B�@���6� ��q+��Y��{�>Rż,��~��S�({<N�ő�Y����\4�rNSQ��Cx&֏!���E����6�}�d��w�`�%��p4��W?�@f�����]J8|�呵#}u�7����sz�&Vn���J���WՕj+aZ�^!Lq�kLwW�C�R�F�#?����As�R�>*����B���r�䠕&"��ӆ)쪏�q�����OX4v�Ć-�t����5��嵅.ia�攻��۳Q�8���<�-s]�}��C _זH�W�@�l�'�.Z	)o� ���^�>7�Vq	l�*����mW���W���V�]�eU��~a'�lQ7��N�|�-խT�.o�5&�(~".?b�3�޳Bi��209��"�\�0���j�kSa���.�?K��h�.����[�4t�eA��V��Ṃܱ��?7�!X�������5b_�I�����ѕ����u7t�	�n�������ńah��Ɛ���-'�V�f�1ό��*��r~c��<D�X䐞��&/�4Kݶ�Q^M3��Z��# �
}wQ�*zw;	��Y�ʉj��r�RQeT�w�d��{��~��yW���l���I�Ĩt���g
�
7b�dU�ݜ�����v����};�b�aqd��<�4s�Z#6��/0�g/%��-��U�V�]��0ػ��8�ǚ�������z�:��6�te�.k�����   LocT   L      ;      C   1      8   -      /   +      ,            
      Attr   (�l   hd vsnl   n oz⚏���cY��jjCInf   ��l   hd optionsl   d 
debug_infojhd versionk 6.0.3hd timehb  �aaaa/ahd sourcek 8/Users/donwb/dev/siteholder/deps/plug/lib/plug/router.exj  Abst  ��P  ��x��Mo��u��ZY��I��T��C�ص�^�*��-�UŮ�,w9Zq�K�$W��q\ l8�"h4@���"��|�o�3.9of��]Y�>h�o����7�٬k�ӶzOQێk�W�:߯�j��lj�y�um�=t�Z�jss`�}���0LE���,�v��3Z����m�������sV�=g���}m�3b���Ԇ�[����n��~�����5�&�2�����{��k�PQ�hô��ө(j�<>=z<tt�;�.R���cm��&�� ���jk��<�u�+���Y����٤- ��kZ�nm��X\���
q��!������RL~{�O(�nl�X43�cE�7{�:�lֹ�6<�6��k��Z�9�ƺ�b�١�'��W]s@�'�� �#Rۀ-�1%O� ��ۄ��@���3M {�P]})����q���Epy���;���b�u�V���P�����F"]�AМn������9��?,��14H�V��>(��k�Rz�P b�5ҐK߬�7�$�Yj�X+6ʆ���Z/�{_�Y�~K�23�,��1)?�W�zQ����蘟��������Y/*��]�~���>?���*(���o��*���3����`��n$��Ɩj{���	b���o�?[6�$�ފ�jt�4v �S^�D-IZ%R���0Q�42ܼ �f��>���k��X`��'�Yмo�z��E����D^��G�� AΗb�/����:��hcJB�ȋ�kE�=F������oy�y�]W���(��1v)a��A�C��)QM�Q��@�5�����8����Q��}X@hpF���l��v���h��L�S�޼�ߤ��[$��j�sƃ�o��wEV�~��}��s�h��@���@�J@ �������2PĨ���1|��v�?�Xo��G<`[?`�\ y�I��Ȃ/�K�p �����%�� Л�#>~��NԻH�',���"�M	]��+�����'�|�o�|������a�X���@8��B����M��u�H��xaw�M�H�t�_�`a�|���6P��#��@��+g�z�x�c
$�Z��U>�#����_�����/����q_::����CQ?���,�C�\������c��˯���-iPVwCv�^K���Kd�EYL��}�%��(�r�,��:CCb�>�3"*ڑ� ƐI��b�_q��L�G��Rr�Q�4�-��؅M�"~q��-�<�dlE&1�p.�̣p4-�龮:�Q���R��P��� W�ਇ-ń~�e�y�i�wX!Q�n� ���hA�?Z��B��.$��"��8?bW/F,J�Y��g1ޑ�Es�w`�;��΁8`H�����U���8��k*�Wb���ݿJ��>�j��^�{JE54?��+�uΐq�"�Am��D��G�lN�B=
��l�O�R̾�ØY~�9THK/�a�X��8Ē��ǀ��I4>�4�Bw�!�i�2����e!@\fg�.q�ˬ� <4L*�Y8Y�M�WB*.�� '�\�\��1D�p�� ��l���<��=�NGG>�$��9Ԋ�^�FC���������n��X�hY� �A\&�[�)���BM�.v:�qx��Ez��aI��U���v�q8�I/�ss�+�&y�j��Y:{|t���p���K�����I�؋��Lc�� ����l��XЖ�o�.�VK��.q�w�0D�c���:�
����6\šmO�_P��UNN�9��)�F4���b�w5�xW#��I��1�~��AЊOr�0h�\��4��&ʁ�V8T����V��f��փ�&��d�^�2m]��Ȇ�%����4�^��K�����5�%Ό��D��I�������x��Z��	qN����c7���&!/㒧$��M(s���|ʃ�M�3�&��P[²p�UmR���J�n��bdp�(,Z̭��{��E�S)5-�����;,�����4�F��p��}T��Z�Rg����j�Qk�$����O���zyL��oѺZO�]9�Z8�"3���} ��U9��a� W��b��2W��x:gmUwP��?�A��E�; ýd�f���� 9�ڥT2;����SA}D��hZ�k�FW�ݭ���-�q�v�#�<�����8�uƴ��q�m�þ�Ջ(�ɇ����Y��^��v(��O<>cU����Nz$%Or�3*q֍�}ޙU�(~T�;�s$����rfZt�.� �:�A�o��>�K�f�R��L>�F���=��$����-ծ��"s�(��TTr$��{>[��$!;�J�xb�/������&l�w�����l�ܮ�MmW�Mm7~���POc��"m#�O�ΟJ;��G�	�K�1�	W0`�'M����D���{QE�'\��	4OI��I��$�|���}3��sxB��bC}+���l��졠���(t4�����_�l�q�]t�@AК���7,b�Z�=��Ўhh���HL��NvL��� /�[$���?SO+3z�IF����>�ɸ��E �Wf��Wd������_ͧ��	���!�I~r1�<�5�ǿ�a懛�?q�����#��R��E����X9�W/�T�2�hl�3���>�s�Q��}�y�Za�򲪠��Tϩ�J�T9eM�}5䙻�cot9���g�۠Nh�RO��X��e�]l�j�JS	�
��4Ѥ�Y��r&^?Lh��x��s��jU���.7��Q��)	�$�*�I������Bj��U����(M��4s*+sj|�v%E���P��б��W���;M��>��?�V�烕)|��2i
�b�;VUV�Ǣ�9�rE������-�UKr38���e4?~eؿ���/*��4���6�q�.=p:ZDA���))��W��8��r}?^�]e���.��OO�.��Ee3�'��v=��6� Wb�?���W�T��2��� ��%�q��ջGP�D�h�d�-)����o`��-dۺ��������jꬤ����⬀"i�>͝Z�3��+v���+�FJ y��K�k#��K�N�;-�)"�ZpB�����[��EG7�qۖ-@�7�ݐ�b� b�BY����mj;��_p�e���:��:�a�+��stȺԟ%מ�k�kO�� ���h{(�u�V�һ�躛�ᙉɀ���w����9J��o���|yB{���M�������&���iUt3BE7#o��ҐH�wyR~]Ȧ�1bEu��-��=Z���-ͻ�@�խXY�
�K��)��9:oÑUr`�m�6"��fE��}��y9��h��<.�i���g��:<�W���շ�q�����.����ߙ}'�]�Hoչ�_����ҝ�[��G=�Q"����ߖ��p��ǿ�#d�]��x׏r����;�O��5���n�r�J�}L�ՎmT�S9�B�g��E��c����Ф74���U��\�MҼ�����N)�v�x�az�|Ȝ�V��L��ω��K}���R��ܐ6��f\�����]���Aǡ���p|y�	������b���cߚ�-��s���^>Wy�׎�ȑp�2�	��S���j�fx��/s�,6<Qu�8�HkэT�d��2����y��jk\�^,9-�p��� u��a� ����Y����F�:���e]���ݤ?��Ȱ�C0��v�+��aS�d���W"�I�D؋���Y(�gZ�}d��3��g�Ο����	B,[�U��y}��j=�j�������E��P��b$S�c��z� FI)�\��;J_����.e�M�l�m���AY���G���+���jD�F���j,���j�!�� �-K@?	+��J� _����+�s�\K��Z��Zq}��C0K�BJ>&�.~��a�d�yʝ�^�/��Ĉ5�̇�r��^�]N�@9	D���ٲ%����`T_Line   o           9   #   	�))w)|)�)�	�	�	�)	�)�)�)G)H)I)J)K)+)�)�	�)Q)T)V)W)Y)\)a)b)R	�)m)n)p lib/plug/router.ex 